$fn = 100;

origalVoronCLipFileName = "STL/[a]_z_belt_clip_upper_x4.stl";
dxfFileName             = "DXF/original retainer projection 01.dxf";
baseLayerName           = "base";
lowerCatchLayerName     = "catchBottom";
upperCatchLayerName     = "catchTop";

baseZ = 2;
catchLowerZ = 3;
catchUpperZ = 3;


module projector(){
  projection()
    import(origalVoronCLipFileName);
}
//projector();

module base(){
  linear_extrude(baseZ)
    import(dxfFileName,layer=baseLayerName);
}
//base();

module catch(){
  linear_extrude(catchLowerZ)
    import(dxfFileName,layer=lowerCatchLayerName);
  translate([0,0,catchLowerZ])
    linear_extrude(catchUpperZ)
      import(dxfFileName,layer=upperCatchLayerName);
}
//catch();

module make(){
  base();
  translate([0,0,baseZ])
    catch();
}
make();