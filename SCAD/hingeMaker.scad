$fn = 100;


hingeStlFileName = "../../Voron-2-from-VoronDesign/STLs/Panel_Mounting/Front_Doors/door_hinge_x6.stl";

splitHingeFileName = "/home/bob/Desktop/Making/3DPrinting/Voron/voron_2.4/STL/Door Hinges/door_hinge_m3_axis_split.stl";

dxfBaseFileName = "/home/bob/Desktop/Making/3DPrinting/Voron/voron_2.4/DXF/DoorHinges/hinges00.dxf";

baseFixLayerName = "baseFix";

dxfMobileFileName = "/home/bob/Desktop/Making/3DPrinting/Voron/voron_2.4/DXF/DoorHinges/hinges01.dxf";

mobileFixLayerName = "mobileFix";

module getSTL(){
  translate([-100,-110,0])
    import(hingeStlFileName);
}

module hole(){
  translate([0,0,3])
    rotate([0,90,0])
      cylinder(50,1.7,1.7);
}

module hinge(){
  difference(){
    getSTL();
    hole();
  }
}
//hinge();

module splitHinge(){
  translate([-130,-94,0])
    import(splitHingeFileName);
}
//projection()
//rotate([0,-90,0])
//splitHinge();

module baseFix(){
  rotate([0,90,0])
    linear_extrude(40)
      import(dxfBaseFileName,layer=baseFixLayerName);
}
module base(){
  intersection(){
    baseFix();
    splitHinge();
  }
}
base();

module mobileFix(){
  //rotate([0,90,0])
    linear_extrude(40)
      import(dxfMobileFileName,layer=mobileFixLayerName);
}
//mobileFix();

module mobile(){
  difference(){
    intersection(){
      translate([0,42,0])
      cube([50,30,20]);
      splitHinge();
    }
    mobileFix();
  }
}
mobile();
