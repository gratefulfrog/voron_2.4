$fn=100;

//////////////////////////////////
/// DXF Params
//////////////////////////////////
DFXFileName = "../DXF/ADXL345_Support.dxf";

ADXL345_pcb_Layer    = "ADXL345_pcb";
Holes_layer          = "Holes";
M3_SHCS_layer        = "M3_SHCS";
RaisedHigh_layer     = "RaisedHigh";
RaisedLow_layer      = "RaisedLow";
Recessed_layer       = "Recessed";
Slots_layer          = "Slots";
SupportContour_layer = "SupportContour";


//////////////////////////////////
/// Z distances
//////////////////////////////////
MinZ      = 1.5;
RecessedZ = 3.5;
SupportZ = RecessedZ + MinZ;
SlotZ    = RecessedZ;
LowZ     = 2.5;
HighZ    = LowZ + MinZ;
MaxZ = 20;

//// makers
//rotate([0,180,0])
  //contour();
//translate([0,0,SupportZ])
  plate();
//translate([0,0,SupportZ+LowZ])
  //ADXL345PCB();

///////////////////////////////////////
///////////////////////////////////////

module contour(){
  difference(){
    union(){
      linear_extrude(SupportZ)
        import(DFXFileName,layer=SupportContour_layer);
      translate([0,0,-RecessedZ])
        linear_extrude(RecessedZ)
          import(DFXFileName,layer=Recessed_layer);
    }
    union(){
      translate([0,0,-MaxZ*0.5])
        linear_extrude(MaxZ )
          import(DFXFileName,layer=Holes_layer);
      linear_extrude(RecessedZ )
          import(DFXFileName,layer=Slots_layer);
    }
  }
}
//contour();
module plate(){
  difference(){
    union(){
      linear_extrude(LowZ)
        import(DFXFileName,layer=RaisedLow_layer);
      linear_extrude(HighZ)
        import(DFXFileName,layer=RaisedHigh_layer);
    }
    translate([0,0,-MaxZ*0.5])
        linear_extrude(MaxZ )
          import(DFXFileName,layer=Holes_layer);
  }
}
//translate([0,0,SupportZ])
  //plate();

module ADXL345PCB(){
   linear_extrude(MinZ)
        import(DFXFileName,layer=ADXL345_pcb_Layer);
}
//translate([0,0,SupportZ+LowZ])
  //ADXL345PCB();