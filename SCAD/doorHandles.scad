$fn = 100;

hingeFilePath   = "/home/bob/Desktop/Making/3DPrinting/Voron/Voron-2-from-VoronDesign/STLs/Panel_Mounting/Front_Doors/"; 
hingeAFileName  = str(hingeFilePath,"handle_a_x2.stl");
hingeBFileName  = str(hingeFilePath, "handle_b_x2.stl");

hingeHeight      = 7;
hingeCushionCut  = 6.5;
cushionAddHeight = 4.8-2*1.5;

function getHingeFileName(letter) = search(letter,"aA") !=[] ? hingeAFileName : hingeBFileName;

module getHeight(){
  projection()
    rotate([0,-90,0])
      addCushion("a");
      //import(hingeAFileName);
}
//getHeight();

module cushionProjection(AorB){
  projection(cut=true)
    translate([0,0,-hingeCushionCut])
      import(getHingeFileName(AorB));
}
//cushionProjection();

module addCushion(AorB) {
  union(){
    translate([0,0,hingeHeight])
      linear_extrude(cushionAddHeight)
        cushionProjection(AorB);
    import(getHingeFileName(AorB));
  }
}
module makeA_B(){
  translate([-100,0,0])addCushion("B");
  addCushion("A");
}
makeA_B();