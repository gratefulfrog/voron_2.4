$fn=100;

//////////////////////////////////
/// DXF Params
//////////////////////////////////
DFXFileName = "../DXF/pi-camera-support.dxf";

AnglePlates_Layer    = "AnglePlates";
Holes_layer          = "Holes";
Support_layer        = "Support";


//////////////////////////////////
/// Z distances
//////////////////////////////////
Z       = 20;
cameraZ = 27;
MaxZ    = 200;

//// makers
//rotate([0,180,0])
  //contour();
//translate([0,0,SupportZ])
  //plate();
//translate([0,0,SupportZ+LowZ])
  //ADXL345PCB();

///////////////////////////////////////
///////////////////////////////////////

module plates(){
  difference(){
    linear_extrude(Z)
      import(DFXFileName,layer=AnglePlates_Layer);
    holeColumn();
  }
}
plates();
//contour();
module holeColumn(){
  translate([0,0,Z/2.0])
    rotate([90,0,0])
    linear_extrude(MaxZ,center=true)
      import(DFXFileName,layer=Holes_layer);
}
//holeColumn();


module CameraSupport(){
  translate([0,0,-(cameraZ-Z)/2.0])
   linear_extrude(cameraZ)
        import(DFXFileName,layer=Support_layer);
}
//CameraSupport();