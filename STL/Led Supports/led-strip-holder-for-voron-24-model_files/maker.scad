$fn = 100;

STLFileName =  "led_strip_holder_300mm.stl";
endCutDXFFileName = "DXF/endCut.dxf";
endCutLayerName = "0";
connectorX = 15;
connectorY = 16;
connectorZ = 5.5;
ledPlateDeltaZ = 3;
endThickness = 10+3;

module getIt(){
  translate([-202.284,155,0])
    rotate([0,0,-45])
    import(STLFileName);
}
//getIt();

module connector(){
  cube([connectorX,connectorY,connectorZ]);
}
/*translate([connectorX+3,0,3+ledPlateDeltaZ])
rotate([0,-20,0])
translate([-connectorX,0,0])
connector();
*/

module getEndCutDxf(){
  //rotate([-90,0,0])
  //translate([0,0,-1])
  //hull()
  //linear_extrude(endThickness)
  difference(){
    projection(true)
      translate([0,0,-1])
        rotate([90,0,0])
          getIt();
    projection(true)
      translate([0,0,-10])
        rotate([90,0,0])
          getIt();
  }
}
//getEndCutDxf();

module getEndCut(){
  import(endCutDXFFileName,layer=endCutLayerName);
}

module endCut(){
  rotate([-90,0,0])
    translate([0,0,-1])
      linear_extrude(endThickness)
        getEndCut();
}


/*
//hull()
 difference(){
    projection(true)
      translate([0,0,-1])
        rotate([90,0,0])
          getIt();
  projection(true)
      translate([0,0,-10])
        rotate([90,0,0])
          getIt();
 }
*/
module cutItR(){
  difference(){
    getIt();
    endCut();
  }
}

//cutItR();

module cutItL(){
  difference(){
    translate([0,-310,0])
      cutItR();
    mirror([0,1,0])
      endCut();
  }
}
cutItL();